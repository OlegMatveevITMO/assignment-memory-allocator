#include "mem.h"
#include "mem_internals.h"
#include "mem_debug.h"

void start_test_pint(int test_num){
    printf("Test №%d\n", test_num);
}

int main(){
    start_test_pint(1);
    void* heap = heap_init(10000);
    debug_heap(stdout, heap);

    start_test_pint(2);
    void* block1 = _malloc(1);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    _malloc(1000);
    _malloc(1000);
    debug_heap(stdout, heap);

    start_test_pint(3);
    _free(block1);
    debug_heap(stdout, heap);

    start_test_pint(4);
    _free(block2);
    _free(block3);
    debug_heap(stdout, heap);

    start_test_pint(5);
    _malloc(10000);
    _malloc(10000);
    debug_heap(stdout, heap);

    start_test_pint(6);
    _malloc(134217711);
    _malloc(134217711);
    debug_heap(stdout, heap);

    free_heap();
}

